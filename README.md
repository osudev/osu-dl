# **THE PROJECT WAS DEPRECATED. PLEASE USE [osu!cli](https://bitbucket.org/osudev/osu-cli).**

# **此项目已被祈免。请使用[osu!cli](https://bitbucket.org/osudev/osu-cli)。**

#

[中文说明请点击此处](#markdown-header-readme-zh)

osu! Beatmap Batch Downloader
========

This tool helps you download osu! beatmaps through mirrors and the official website.

When you provide a target username. The program grab the list of all his/her favorite and created beatmaps. Then it iterate a list of mirrors (Bloodcat, loli.ol, osu.uu.gl, and osu.ppy.sh) to find the first existence and download it.

You may also specify your osu! `Songs` folder where you store all your installed beatmaps. The program would check if the beatmap exists in the folder, and skip it if it does. By default the program will check at least the download folder for duplication.

Usage
--------

1. Clone the repository
2. Run `gem install nokogiri`

```text
Usage: ruby cmd.rb [options]
    -u, --user [USERNAME]            Username
    -d, --songsdir [PATH]            Song folder under osu! game directory
    -o, --outdir [PATH]              Beatmap download folder
    -f, --no-fav                     Ignore user favorites beatmaps
    -r, --no-ran                     Ignore user ranked beatmaps
    -p, --no-pen                     Ignore user pending beatmaps
    -g, --no-gra                     Ignore user graveyarded beatmaps
    -O, --use-osu                    Allow download through osu! official site (login required)
    -h, --help                       Show help
```

Example
----------

```bash
./cmd.rb -d ~/Applications/osu\!.app/Songs -O -u peppy
# Type in your osu! username and password if you used -O option to allow download from official site.
```

Windows Notes
--------------------

As many people aksed me for a Windows version, I am working on a native Windows osu! helper application, but it's still under development. I can give those people a quick fix to run this script on Windows.

1. Download and install [Ruby for Windows](http://rubyinstaller.org/downloads/) don't include spaces in the installation path. Check "Install Td/Tk support",  "Add Ruby executable to your PATH", and "Associate .rb and .rbw files with this Ruby installation"
2. Download and install the "With Administrator Privileges (free)" version of [cURL for Windows](http://www.confusedbycode.com/curl/#downloads)
3. download and execute this [registry configuration](https://gist.github.com/RixTox/ecad21cbc73b5175b4a3/raw/9be97604da79a9d3a01e098c76c0a155bc624e81/Add_Open_Command_Window_Here_as_Administrator.reg) to add a "Open Command Window Here as Administrator" option in your file explorer right click menu (just for convenience)
4. Download and extract [this osu! download script](https://bitbucket.org/osudev/osu-dl/get/master.zip)
5. Open your extracted folders until you see a pile of `.rb` files. Then right click at the blank area of your file explorer, click "Open command window here as Administrator"
7. Create a folder somewhere to be used for save the downloaded beatmaps. Copy down the folder path. For instance `E:\osu\downloads`
8. Now you may start to download beatmaps with this script. For example I want to download all the beatmaps of Cherry Blossom to `E:\osu\downloads` and filter out those I already had in my osu! Songs folder `D:\osu!\Songs`. You may execute the following command in the previous command window: `ruby cmd.rb -d "D:\osu!\Songs" -o "E:\osu\downloads" -O -u "Cherry Blossom"` The program will ask for your osu! account authentication, just fill in and the download will begin.

README-zh
=======

这款工具用于从osu!官网以及几大镜像站(Bloodcat, loli.ol, osu.uu.gl, osu.ppy.sh)批量下图。目前此工具并不具备搜索功能，它能做的只是根据输入的用户名列表获取玩家的Favorite、Ranked、Pending和Graveyarded曲目列表，然后根据本地的osu!歌曲目录排除掉已存在的曲目，然后进行批量下载。该程序会优先从镜像站下载，如果都下载失败可以选用osu!官网，不过用户需要设置osu!的用户名和密码以获取下载权限。

Windows使用帮助
--------------

鉴于很多人要求提供Windows版，而我正在写的一套Windows下的osu!插件还没有完成，先提供一套临时的应对方案在Windows下使用本程序。

1. 下载安装[Ruby for Windows](http://rubyinstaller.org/downloads/) 注意安装目录中不要包含空格，并勾选Install Td/Tk support, Add Ruby executable to your PATH, Associate .rb and .rbw files with this Ruby installation
2. 下载安装[cURL for Windows](http://www.confusedbycode.com/curl/#downloads) 根据你的系统位数选择With Administrator Privileges (free)版本。
3. 下载执行这个[注册表配置文件](https://gist.github.com/RixTox/ecad21cbc73b5175b4a3/raw/9be97604da79a9d3a01e098c76c0a155bc624e81/Add_Open_Command_Window_Here_as_Administrator.reg) 添加执行命令行选项到系统右键菜单（这个只是为了免去我下面的废话解释用的）
4. 下载解压[本脚本程序](https://bitbucket.org/osudev/osu-dl/get/master.zip)
5. 打开解压出来的目录直到你看到一堆`.rb`文件，右键空白的地方，然后选择Open command window here as Administrator打开一个命令行窗口
6. 输入`gem install nokogiri --pre`然后按回车执行
7. 去建立一个存放下载好的图的目录，然后把目录地址拷贝下来，比如`E:\osu\downloads`
8. 现在你就可以使用本程序下图了。比如我要下Cherry Blossom所有的鬼畜图，就在刚刚的命令行窗口执行`ruby cmd.rb -d "D:\osu!\Songs" -o "E:\osu\downloads" -O -u "Cherry Blossom"`，把`-d`后面的目录替换成你的osu!歌曲目录，把`-o`后面的目录替换成你要下载保存的目录。然后这个程序会提示你输入你的osu!用户名和密码，然后就开始慢慢地下图了啦。

命令详解
---------

```text
Usage: ruby cmd.rb [options]
    -u, --user [USERNAME]            你想下谁的图这里就填谁
    -d, --songsdir [PATH]            osu!歌曲目录
    -o, --outdir [PATH]              下载保存的目录
    -f, --no-fav                     忽略favorites
    -r, --no-ran                     忽略ranked
    -p, --no-pen                     忽略pending
    -g, --no-gra                     忽略graveyarded
    -O, --use-osu                    允许从osu!官网下载（需要登录）
    -h, --help                       显示帮助
```

Bugs
----

有时候由于网络问题，cURL没能成功下载到图，或者服务端的图挂掉了，会变成一个数字文件名的文件。这个Bug我还没什么头绪解决，因为涉及到读取cURL运行时的数据，可能只有调动态接口才能解决。

因为Windows的奇葩文件系统NTFS的原因，有些图的文件名字中包含一些特殊字符（如星号）的话就无法正常下载，这个问题要解决的话需要改动大量的代码。。还是去写我那个插件写好了。

么么哒~