require 'uri'

class NoAvailabeDownloadService < Exception
end

class Downloader

  def initialize(conf = {})
    @cookiejar = File.expand_path(File.join(File.dirname(__FILE__), "cookie.jar"))
    @dir = conf[:dir]
    @maplib = conf[:maplib]
    @osu = conf[:osu]
    @login = conf[:login]
    @to_null ||= (
      host_os = RbConfig::CONFIG['host_os']
      case host_os
      when /mswin|msys|mingw|cygwin|bccwin|wince|emc/
        "2> NUL"
      when /darwin|mac os/
        "2> /dev/null"
      when /linux/
        "2> /dev/null"
      when /solaris|bsd/
        "2> /dev/null"
      else
        raise Error::WebDriverError, "unknown os: #{host_os.inspect}"
      end
    )
    @mkdir ||= (
      host_os = RbConfig::CONFIG['host_os']
      case host_os
      when /mswin|msys|mingw|cygwin|bccwin|wince|emc/
        "mkdir"
      when /darwin|mac os/
        "mkdir -p"
      when /linux/
        "mkdir -p"
      when /solaris|bsd/
        "mkdir -p"
      else
        raise Error::WebDriverError, "unknown os: #{host_os.inspect}"
      end
    )
  end

  def maps(list)
    list.each do |the_map|
      map the_map
    end
  end

  def map(the_map)
    id = the_map.id
    if @maplib.exist?(id)
      puts "Map #{id} exists in your songs folder. Skipped."
      return
    end
    if bloodcat_exist? id
      download "http://bloodcat.com/osu/m/#{id}"
    elsif (loli_id = get_loli_id_by_set(id).to_s) != "0"
      loli_url = get_loli_url(loli_id)
      file_name = URI::decode(File.basename(URI::parse(loli_url).path))
      download loli_url, file_name
    elsif uu_gl_exist? id
      download "http://osu.uu.gl/s/#{id}"
    elsif @osu
      login @login[:user], @login[:pass]
      download "http://osu.ppy.sh/d/#{id}"
    else
      raise NoAvailabeDownloadService
    end
  end

  def bloodcat_exist?(id)
    headers = curl("http://bloodcat.com/osu/m/#{id}", '-I', true)
    !headers[/Content-Type: application\/octet-stream/im].nil?
  end

  def uu_gl_exist?(id)
    begin
      headers = curl("http://osu.uu.gl/s/#{id}", '-I', true)
      !headers[/Content-Type: application\/octet-stream/im].nil?
    rescue
      false
    end
  end

  def get_loli_id_by_set(id)
    begin
      curl("http://loli.al/osu.osp?action=check&id=#{id}&type=set", '', true).chomp
    rescue
      "0"
    end
  end

  def get_loli_url(id)
    curl("http://loli.al/d/#{id}/", '-I', true)[/(?<=Location: ).*/].chomp
  end

  def login(user, pass)
    curl("https://osu.ppy.sh/forum/ucp.php?mode=login", "--data \"username=#{user}&password=#{pass}&login=login\" -c \"#{@cookiejar}\"", true)
  end

  def download(url, file_name = nil)
    `#{@mkdir} #{@dir}`
    Dir.chdir @dir
    p "Downloading #{url}", "Save to #{@dir}"
    curl(url, "-L -J #{file_name.nil? ? '-O' : "-o \"#{file_name}\""} -b \"#{@cookiejar}\" -c \"#{@cookiejar}\"", false)
  end
    
  def curl(url, options, to_null)
    cmd = "curl #{options} \"#{url}\" #{to_null ? @to_null : ''}"
    `#{cmd}`
  end


end