#!/usr/bin/env ruby

require './maplib'
require './user'
require './downloader'
require 'optparse'
require 'io/console'

class UserNotGiven < Exception
end

def get_username(prompt="Username: ")
  print prompt
  STDIN.readline(&:gets).chomp
end

def get_password(prompt="Password: ")
  print prompt
  STDIN.noecho(&:gets).chomp
end

@conf = {
  :dir => File.expand_path(File.join(File.dirname(__FILE__), 'download')),
  :fav => true,
  :ran => true,
  :pen => true,
  :gra => true,
  :osu => false,
  :login => {}
}

opt_parser = OptionParser.new do |opts|
  opts.banner = "Usage: cmd.rb [options]"

  opts.on("-u", "--user [USERNAME]", "Username") do |v|
    @conf[:user] = v
  end

  opts.on("-d", "--songsdir [PATH]", "Song folder under osu! game directory") do |v|
    @conf[:songdir] = v
  end

  opts.on("-o", "--outdir [PATH]", "Beatmap download folder") do |v|
    @conf[:dir] = v
  end

  opts.on("-f", "--no-fav", "Ignore user favorites beatmaps") do
    @conf[:fav] = false
  end

  opts.on("-r", "--no-ran", "Ignore user ranked beatmaps") do
    @conf[:fav] = false
  end

  opts.on("-p", "--no-pen", "Ignore user pending beatmaps") do
    @conf[:fav] = false
  end

  opts.on("-g", "--no-gra", "Ignore user graveyarded beatmaps") do
    @conf[:fav] = false
  end

  opts.on("-O", "--use-osu", "Allow download through osu! official site (login required)") do
    @conf[:osu] = true
  end

  opts.on("-l", "--login-user [USER]", "Your osu! login username") do |v|
    @conf[:login][:user] = v
  end

  opts.on("-p", "--login-pass [PASS]", "Your osu! login password") do |v|
    @conf[:login][:pass] = v
  end

  opts.on("-h", "--help", "Show help") do
    puts opts.help
  end
end.parse!

unless @conf.has_key? :user
  raise UserNotGiven
end

@conf[:maplib] = MapLib.new @conf

if @conf[:osu]
  @conf[:login] = {
    :user => @conf[:login][:user] || get_username,
    :pass => @conf[:login][:pass] || get_password
  }
end

dl = Downloader.new @conf

user = User.new @conf[:user]

if @conf[:fav]
  dl.maps user.maps[:fav]
end

if @conf[:ran]
  dl.maps user.maps[:ran]
end

if @conf[:pen]
  dl.maps user.maps[:pen]
end

if @conf[:gra]
  dl.maps user.maps[:gra]
end
