require './map.rb'
require 'net/http'
require 'uri'
require 'nokogiri'

class User

  attr_accessor :name, :id, :maps
  
  @@user_profile = "http://osu.ppy.sh/u/%s"
  @@user_maps = "http://osu.ppy.sh/pages/include/profile-beatmaps.php?u=%s"
  
  def initialize(name)
    @maps = {
      :fav => [],
      :ran => [],
      :pen => [],
      :gra => []
    }

    @@profile_html = get @@user_profile % URI::encode(name)
    not_found_node = @@profile_html.css ".mainbody .mainbody2 .content .content-with-bg .paddingboth h2"
    if not not_found_node.empty? and not_found_node[0].text[/The user you are looking for was not found/]
      throw :user_not_found
    end
    @name = get_name
    @id = get_id

    @@maps_html = get @@user_maps % @id
    get_maps
  end


  private
  
  def get(url)
    Nokogiri::HTML Net::HTTP.get URI.parse url
  end

  def get_name
    @@profile_html.css(".profile-username")[0].text.strip
  end

  def get_id
    @@profile_html.css("script").each do |script|
      id = script.text[/(?<=userId = )\d+/]
      return id.strip unless id.nil?
    end
    nil
  end

  def get_maps
    [
     [:fav, "#beatmapsFavourite"],
     [:ran, "#beatmapsRanked"],
     [:pen, "#beatmapsPending"],
     [:gra, "#beatmapsGraveyard"]
    ].each do |args|
      col, id = args
      puts "Processing #{col} maps"
      @@maps_html.css(id + " .prof-beatmap").each do |map|
        link = map.css(".h a")[0]
        @maps[col.to_sym] << Map.new(link['href'])
      end
    end
  end
end
