require 'mongoid'
Mongoid.load!('./mongoid.yml', 'development')

class Song
  include Mongoid::Document

  field :song_id
  field :title
  field :artist
  field :add_date, :type => Time
  embeds_many :maps

end

class Map
  include Mongoid::Document

  field :map_id
  field :mapper
  field :tag
  field :hash
end

map = Map.create({
                   :map_id => "3456666",
                   :mapper => "Tor",
                   :tag => "Insane",
                   :hash => "di2uh908h2q0"
                 })

song = Song.create({
                  :song_id => "280993",
                  :title => "Hello, Song",
                  :artist => "Sking",
                  :add_date => Time.now,
                  :maps => [map]
                })


